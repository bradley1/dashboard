import React, { Component } from 'react'
import PieComponent from './components/Pie.js'
import LineComponent from './components/Line.js'
import axios from 'axios'
import './App.css'
import io from 'socket.io-client'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

export default class App extends Component {

  constructor (props) {
    super(props)

    this._getData = this._getData.bind(this)
    this._loaded = this._loaded.bind(this)

    this.state = {
      pieState: [],
      pieState2: [],
      lineState: [],
      initialLoad: false
    }
  }

  componentDidMount () {
    this._getData()
    const socket = io('https://api.vtlibraries.us')
    socket.on('connect', () => {
      console.log('Client Connected')
    })

    socket.on('update', () => {
      this._getData()
      console.log('Data Updated!')
    })
  }

  _getData () {
    axios.get('https://api.vtlibraries.us/designstudio')
      .then(res => {
        this.setState({ pieState: res.data.pie1, lineState: res.data.line1, pieState2: res.data.pie2, initialLoad: true })
      })
  }

  _loaded () {
    if (!this.state.initialLoad) {
      return (
        <div className='mainContainer'>
          <Segment>
            <Dimmer active>
              <Loader size='massive' inline='centered'>Loading</Loader>
            </Dimmer>
          </Segment>
        </div>
      )
    } else {
      return (
        <div className='mainContainer'>
          <Segment raised>
            <div className='pieContainer'>
              {this.state.pieState.length && <PieComponent data={this.state.pieState} />}
            </div>
          </Segment>
          <Segment raised>
            <div className='pieContainer'>
              {this.state.pieState.length && <PieComponent data={this.state.pieState2} />}
            </div>
          </Segment>
          <Segment raised>
            <div className='lineContainer'>
              {this.state.lineState.length && <LineComponent data={this.state.lineState} />}
            </div>
          </Segment>
        </div>
      )
    }
  }

  render () {
    return (
      <div>
        {this._loaded()}
      </div>
    )
  }
}
