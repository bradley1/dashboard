import React, { Component } from 'react'
import { ResponsiveLine } from 'nivo'

export default class LineComponent extends Component {
  render () {
    return (
      <ResponsiveLine
        data={this.props.data}
        margin={{
          'top': 50,
          'right': 50,
          'bottom': 60,
          'left': 80
        }}
        minY='auto'
        maxY='auto'
        stacked
        curve='linear'
        axisBottom={{
          'orient': 'bottom',
          'tickSize': 5,
          'tickPadding': 10,
          'tickRotation': 0,
          'legend': 'Printer Name',
          'legendOffset': 40,
          'legendPosition': 'center'
        }}
        axisLeft={{
          'orient': 'left',
          'tickSize': 5,
          'tickPadding': 10,
          'tickRotation': 0,
          'legend': 'Number of Hours',
          'legendOffset': -50,
          'legendPosition': 'center'
        }}
        enableGridX
        enableGridY
        colors={["#949595", "#73afb6", "#851b3e", "#3b6e8f", "#9bb68f", "#ff6600"]}
        colorBy='id'
        lineWidth={2}
        enableDots
        dotSize={10}
        dotColor='inherit:darker(0.3)'
        dotBorderWidth={2}
        dotBorderColor='#ffffff'
        enableDotLabel
        dotLabel='y'
        dotLabelYOffset={-20}
        animate
        motionStiffness={90}
        motionDamping={15}
        isInteractive
        enableStackTooltip
    />
    )
  }
}
