import React, { Component } from 'react'
import { ResponsivePie } from 'nivo'

export default class PieComponent extends Component {
  render () {
    return (
      <ResponsivePie

        data={this.props.data}
        margin={{
          'top': 81,
          'right': 80,
          'bottom': 80,
          'left': 80
        }}
        sortByValue={false}
        innerRadius={0.55}
        padAngle={1}
        cornerRadius={3}
        colors={["#949595", "#73afb6", "#851b3e", "#3b6e8f", "#9bb68f", "#ff6600"]}
        colorBy='id'
        borderWidth={1}
        borderColor='inherit:darker(0.6)'
        enableRadialLabels
        radialLabel='id'
        radialLabelsSkipAngle={9}
        radialLabelsTextXOffset={6}
        radialLabelsTextColor='#333333'
        radialLabelsLinkOffset={0}
        radialLabelsLinkDiagonalLength={16}
        radialLabelsLinkHorizontalLength={24}
        radialLabelsLinkStrokeWidth={1}
        radialLabelsLinkColor='inherit'
        enableSlicesLabels
        sliceLabel='value'
        slicesLabelsSkipAngle={10}
        slicesLabelsTextColor='#333333'
        animate
        motionStiffness={90}
        motionDamping={15}
        isInteractive
    />
    )
  }
}
