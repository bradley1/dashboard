module.exports = {
  'pie': [
    {
      'id': 'Class project',
      'label': 'Class project',
      'value': 0
    },
    {
      'id': 'Personal project',
      'label': 'Personal project',
      'value': 0
    }],
  'pie2': [
    {
      'id': 'Original design',
      'label': 'Original design',
      'value': 0
    },
    {
      'id': 'Downloaded model',
      'label': 'Downloaded model',
      'value': 0
    },
    {
      'id': 'Modified existing design',
      'label': 'Modified existing design',
      'value': 0
    }],
  'line': [
    {
      'id': 'Estimated Hours per Printer',
      'data': [
        {
          'x': 'Calvin',
          'y': 0
        },
        {
          'x': 'Hobbes',
          'y': 0
        },
        {
          'x': 'Michaelangelo',
          'y': 0
        },
        {
          'x': 'Raphael',
          'y': 0
        },
        {
          'x': 'The Sound',
          'y': 0
        },
        {
          'x': 'The Fury',
          'y': 0
        },
        {
          'x': 'Donatello',
          'y': 0
        },
        {
          'x': 'Scratchy',
          'y': 0
        },
        {
          'x': 'Leonardo',
          'y': 0
        },
        {
          'x': 'Itchy',
          'y': 0
        },
        {
          'x': 'Sagan',
          'y': 0
        },
        {
          'x': 'Tyson',
          'y': 0
        },
        {
          'x': 'Waldorf',
          'y': 0
        },
        {
          'x': 'Statler',
          'y': 0
        }
      ]
    }
  ]
}
