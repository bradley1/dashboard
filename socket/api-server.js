const app = require('express')()
const http = require('http').Server(app)
const rp = require('request-promise')
const definitions = require('./definitions.js')
const io = require('socket.io')(http)

app.get('/', function (req, res) {
  var options = {
    uri: 'https://printtracker.lib.vt.edu/api/index.php/queue',
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true
  }

  rp(options)
    .then(function (data) {
      return process(data)
    })
    .then(function (data2) {
      res.json(data2)
    })
    .catch(function (err) {
      console.log(err)
    })
})

io.on('connection', (socket) => {
  console.log('a user connected')

  socket.on('incoming', () => {
    console.log('Update Triggered')
    io.emit('update')
  })

  socket.on('disconnect', () => {
    console.log('a user disconnected')
  })
})

http.listen(4000, function () {
  console.log('listening on :4000')
})

function process (data) {
  const defcopy = JSON.parse(JSON.stringify(definitions))
  let newData = {'pie1': defcopy.pie, 'pie2': defcopy.pie2, 'line1': defcopy.line}
  for (var i = 0; i < data.length; i++) {
    for (var x = 0; x < newData.pie1.length; x++) {
      if (data[i].purpose === newData.pie1[x].id) {
        newData.pie1[x].value++
      }
    }
    for (var z = 0; z < newData.pie2.length; z++) {
      if (data[i].source === newData.pie2[z].id) {
        newData.pie2[z].value++
      }
    }
    for (var y = 0; y < newData.line1[0].data.length; y++) {
      if (data[i].printer === newData.line1[0].data[y].x) {
        newData.line1[0].data[y].y += data[i].estimated_time
      }
    }
    if (data.length - 1 === i) {
      return newData
    }
  }
}
